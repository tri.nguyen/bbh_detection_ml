
import os
import argparse
import subprocess

from mldet import io
from mldet.config import TRAIN_PARAMS_KEYS

# Parse command line argument
def parse_cmd():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
    parser.add_argument('config', help='Path to config file', type=str) 
    params = parser.parse_args()
    return params

params = parse_cmd()
config = io.parse_config(params.config, 'config')

# Call training script
train_cmd = 'mldet-training ' + io.dict2args(config, TRAIN_PARAMS_KEYS)
print('Run cmd: ' + train_cmd)
print('--------------------------')
subprocess.check_call(train_cmd.split(' '))
