
import os
import argparse
import subprocess

from mldet import io
from mldet.config import TEST_PARAMS_KEYS

# Parse command line argument
def parse_cmd():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
    parser.add_argument('config', help='Path to config file', type=str) 
    params = parser.parse_args()
    return params

params = parse_cmd()
config = io.parse_config(params.config, 'config')

# Call training script
test_cmd = 'mldet-testing ' + io.dict2args(config, TEST_PARAMS_KEYS)
print('Run cmd: ' + test_cmd)
print('--------------------------')
subprocess.check_call(test_cmd.split(' '))
