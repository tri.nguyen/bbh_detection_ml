
import os
import sys
import glob
import logging

logger = logging.getLogger(__name__)

import numpy as np

import torch.utils.data

def print_train():
    ''' Most important function '''
    logger.info('------------------------------------')
    logger.info('------------------------------------')
    logger.info('')
    logger.info('         ..oo0  ...ooOO00           ')
    logger.info('        ..     ...             !!!  ')
    logger.info('       ..     ...      o       \o/  ')
    logger.info('   Y  ..     /III\    /L ---    n   ')
    logger.info('  ||__II_____|\_/| ___/_\__ ___/_\__')
    logger.info('  [[____\_/__|/_\|-|______|-|______|')
    logger.info(' //0 ()() ()() 0   00    00 00    00')
    logger.info('')
    logger.info('------------------------------------')
    logger.info('------------------------------------')

def get_device(device):
    ''' Convenient function to set up hardward '''
    if device.lower() == 'cpu':
        device = torch.device('cpu')
    elif 'cuda' in device.lower():
        if torch.cuda.is_available():
            device = torch.device(device)
        else:
            logging.warning('No GPU available. Use CPU instead.')
            device = torch.device('cpu')
    if device.type == 'cuda':
        total_memory = torch.cuda.get_device_properties(device).total_memory
        total_memory *= 1e-9 # convert bytes to Gb
        logger.info('Use device: {}'.format(torch.cuda.get_device_name(device)))
        logger.info('Total memory: {:.4f} GB'.format(total_memory))
    else:
        logger.info('Use device: CPU')
    return device

def get_checkpoint(model_name, dataset_name, checkpoint=None):
    data_subdir = f'data/models/{dataset_name}/{model_name}'
    if checkpoint is None:    
        for epoch in range(100000):
            temp = os.path.join(data_subdir, f'epoch_{epoch}')
            if not os.path.exists(temp):
                return checkpoint
            checkpoint = temp
    elif isinstance(checkpoint, str):
        return checkpoint
    elif isinstance(checkpoint, int):
        return os.path.join(data_subdir, f'epoch_{checkpoint}')
    return None
    
def get_datasets(datadir, partition=(80, 10, 10), shuffle=True, seed=None, ifo=None):
    
    if (ifo is not None) and (ifo.upper() not in ('H1', 'L1', 'H1L1')):
        raise ValueError(f'ifo {ifo} is not recognized')
    
    if not os.path.exists(datadir):
        raise FileNotFoundError(f'{datadir} not found')

    n_data = len(glob.glob(os.path.join(datadir, 'data_*.pt')))
    n_train = int(partition[0] * n_data / np.sum(partition))
    n_val = int(partition[1] * n_data / np.sum(partition))
    n_test = n_data - n_train - n_val
    
    logging.info(f'Total number of samples: {n_data}')
    logging.info(f'Number of training samples: {n_train}')
    logging.info(f'Number of validation samples: {n_val}')
    logging.info(f'Number of testing samples: {n_test}')
    
    # shuffle 
    if shuffle:
        np.random.seed(seed)
        indices = np.random.permutation(n_data)
    else:
        indices = np.arange(n_data)
    indices_train = indices[: n_train]
    indices_val = indices[n_train : n_train + n_val]
    indices_test = indices[n_train + n_val:]
    
    # define dataset class
    class Dataset:
        def __init__(self, indices, datadir):
            self.datadir = datadir
            self.indices = indices
            
        def __len__(self):
            return len(self.indices)
        
        def __getitem__(self, i):
            idx = self.indices[i]
            data = torch.load(os.path.join(datadir, f'data_{idx}.pt'))
            x = torch.Tensor(data['x'])
            y = torch.Tensor(data['y'])
            if ifo is not None:
                if ifo == 'H1':
                    x = x[:1]
                elif ifo == 'L1':
                    x = x[1:]
            return x, y
        
    train_dataset = Dataset(indices_train, datadir)
    val_dataset = Dataset(indices_val, datadir)
    test_dataset = Dataset(indices_test, datadir)
    
    return train_dataset, val_dataset, test_dataset, seed

def get_parameters(parameter_dir):
    ''' Get parameters into dictionary format '''
    
    n_data = len(glob.glob(os.path.join(parameter_dir, 'data_*.pt')))
    noise_params = {}
    signal_params = {}
    for i in range(n_data):
        params = torch.load(os.path.join(parameter_dir, f'data_{i}.pt'))
        if 'snr' not in params.keys():
            for key, val in params.items():
                if key not in noise_params.keys():
                    noise_params[key] = []
                noise_params[key].append(val)
        else:
            for key, val in params.items():
                if key not in signal_params.keys():
                    signal_params[key] = []
                signal_params[key].append(val)

    return noise_params, signal_params
    