
# All parameters
TRAIN_PARAMS_KEYS = (
    'datadir', 'dataset_name', 'model_name', 'ifo', 'kernels', 'filters',
    'dilations', 'pooling', 'linear', 'bn', 'dropout', 'pooling_type', 'pooling_first',
    'input_shape', 'batch_size', 'max_epochs', 'num_workers', 'lr', 'weight_decay',
    'partition', 'seed', 'log', 'device')
TEST_PARAMS_KEYS = (
    'datadir', 'dataset_name', 'model_name', 'ifo', 'kernels', 'filters',
    'dilations', 'pooling', 'linear', 'bn', 'dropout', 'pooling_type', 'pooling_first',
    'input_shape', 'batch_size', 'num_workers','partition', 'seed', 'log', 'checkpoint', 
    'flag', 'out_dir', 'device')
REAL_EVENT_PARAMS_KEYS = (
    'event_file', 'dataset_name', 'model_name', 'ifo', 'kernels', 'filters',
    'dilations', 'pooling', 'linear', 'bn', 'dropout', 'pooling_type', 'pooling_first',
    'input_shape', 'batch_size', 'num_workers','partition', 'seed', 'log', 'checkpoint', 
    'flag', 'out_dir', 'device')
DATA_PARAMS_KEYS = (
    'x', 'y', 'snr', 'a_1', 'a_2', 'dec', 'geocent_time', 'luminosity_distance', 
    'mass_1', 'mass_2', 'phase', 'phi_12', 'phi_jl', 'psi', 'ra', 'theta_jn', 
    'tilt_1', 'tilt_2')
CONDOR_PARAMS_KEYS = ('job_name', 'universe', 'accounting_group')
ALL_PARAMS_KEYS = list(set().union(
    TRAIN_PARAMS_KEYS, TEST_PARAMS_KEYS, REAL_EVENT_PARAMS_KEYS, 
    DATA_PARAMS_KEYS, CONDOR_PARAMS_KEYS)) 


# Default settings
# dataset settings
DEFAULT_IFO = 'H1L1'

# network settings
DEFAULT_KERNELS = (16, 8, 8)
DEFAULT_FILTERS = (16, 32, 64)
DEFAULT_DILATIONS = (1, 1, 4, 4)
DEFAULT_POOLING = (4, 4, 4, 4)
DEFAULT_LINEAR = (128, 64)
DEFAULT_BN = True
DEFAULT_DROPOUT = 0.8
DEFAULT_POOLING_FIRST = True
DEFAULT_POOLING_TYPE = 'max'

# cuda settings
DEFAULT_DEVICE = 'cuda:0'

# training settings
DEFAULT_BATCH_SIZE = 32
DEFAULT_MAX_EPOCHS = 50
DEFAULT_LR = 1e-3
DEFAULT_WEIGHT_DECAY = 1e-3
DEFAULT_NUM_WORKERS = 16
DEFAULT_PARTITION = 0.9

# DEFAULT_PARAMS_TYPES = {
#     'general': {
#         'datadir': str,
#         'parameter_dir': str,
#         'dataset_name': str,
#         'model_name': str,
#         'ifo': str,
#     },
#     'network': {
#         'kernels': (int,),
#         'filters': (int,),
#         'dilations': (int,),
#         'pool': (int,),
#         'linear_dims': (int,),
#         'bn': bool,
#         'dropout': float,
#         'pool_first': bool,
#         'maxpool': bool, 
#         'checkpoint': str,
#         'input_shape': (int, )
#     },
#     'training': {
#         'batch_size': int,
#         'max_epochs': int,
#         'lr': float,
#         'ld': float,
#         'num_workers': int,
#         'train_frac': float,
#         'seed': int
#     },
#     'scheduler': {
#         'scheduler': str,
#         'milestones': (int, ),
#         'step_size': int,
#         'gamma': float,
#         'last_epoch': int,
#         'mode': str, 
#         'factor': float, 
#         'patience': int, 
#         'verbose': bool, 
#         'threshold': float, 
#         'threshold_mode': str, 
#         'cooldown': int, 
#         'min_lr': float, 
#         'eps': float,
#     },
# }